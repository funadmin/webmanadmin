<?php

use Webman\Event\EventListCommand;

use fun\curd\Curd;
use fun\curd\Menu;
use fun\curd\Install;

return [
    EventListCommand::class,
    // 指令定义
    Curd::class,
    Menu::class,
    Install::class,
];