<?php
declare (strict_types = 1);

namespace addons\{{$addon}}\controller;

use support\Request;
use support\App;
use support\View;
use fun\addons\Controller;


class Index extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index(){

        return fetch();
    }


}

