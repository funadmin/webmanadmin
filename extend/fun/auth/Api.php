<?php
/**
 * FunAdmin
 * ============================================================================
 * 版权所有 2017-2028 FunAdmin，并保留所有权利。
 * 网站地址: https://www.FunAdmin.com
 * ----------------------------------------------------------------------------
 * 采用最新Thinkphp6实现
 * ============================================================================
 * Author: yuege
 * Date: 2019/10/3
 */
namespace fun\auth;


use fun\auth\Send;
use fun\auth\Oauth;
use support\Response;
use support\Request;

/**
 * api 入口文件基类，需要控制权限的控制器都应该继承该类
 */
class Api
{
    use Send;

    /**
     * @var Request
     */
    protected $request;
    /**
     * @var
     * 客户端信息
     */
    protected $clientInfo;
    /**
     * 不需要鉴权方法
     */
    protected $noAuth = [];
    
    protected $member_id = '';

    protected $type ='simple';
    
    /**
     *
     */
    public function __construct()
    {
        $this->type  = Config('api.type','simple');
        
        $this->group =  request()->input('group')?request()->input('group'):'api';
        $this->init();
        if($this->clientInfo){
            $this->member_id = $this->clientInfo['member_id'];
        }
    }

    /**
     * 初始化
     * 检查请求类型，数据格式等
     */
    public function init()
    {
        $class = '\\fun\\auth\\'.ucfirst($this->type).'Oauth';
        $oauth = $class::instance(['noAuth'=>$this->noAuth]);
        $this->clientInfo = $oauth->authenticate();
    }
    /**
     * 空方法
     */
    public function _empty()
    {
       return $this->error('empty method!');
    }
}